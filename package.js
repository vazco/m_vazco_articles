Package.describe({
    summary: "Vazco Articles component"
});

Package.on_use(function (api) {
    api.use([
        'vazco-tools-common',
        'vazco-core',
        'vazco-loader',
        'autoform',
        'collection2'
    ], [
        'client',
        'server'
    ]);

    api.use([
        'templating'
    ], [
        'client'
    ]);

    api.add_files([
        'templates/article.html',
        'templates/form.html',
        'templates/list.html'
    ], [
        'client'
    ]);

    api.add_files([
        'articles.js'
    ], [
        'client',
        'server'
    ]);
});