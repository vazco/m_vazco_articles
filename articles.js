'use strict';

Vazco.loader.articles = {
    // Equivalent of:
    // Router.map(function(){
    //     this.route('article', {path: 'article/:_id'});
    routes: {
        article: {
            path: 'article/:_id'
        },
        articleList: {
            path: 'articleList'
        },
        articleAdd: {
            path: 'articleAdd',
            template: 'articleForm'
        },
        articleEdit: {
            path: 'articleEdit/:_id',
            template: 'articleForm'
        }
    },
    // Equivalent of:
    // Articles = new Meteor.Collection('Articles', {schema: ...})
    collections: {
        Articles: {
            schema: new SimpleSchema({
                title: {
                    label: 'Title',
                    type: String,
                    max: 100
                },
                content: {
                    label: 'Content',
                    type: String,
                    optional: true,
                    max: 5000
                }
            })
        }
    },
    // Equivalent of:
    // Meteor.methods({removeAllArticles: ...})
    methods: {
        removeAllArticles: function () {
            Articles.remove();
        }
    },
    // Equivalent of:
    // Meteor.publish('article', function(id){...});
    publications: {
        article: function (id) {
            return Articles.findOne(id);
        }
    },
    // Equivalent of:
    // Template.articleList.helpers({articles: ...})
    // and
    // Template.articleList.events({'click': ...})
    events: {
        articleList: {
            helpers: {
                articles: function () {
                    return Articles.find();
                }
            },
            events: {
                'click': function () {
                    alert('hej!');
                }
            }
        },
        article: {
            helpers: {
                article: function () {
                    return Articles.findOne(Router.current().params._id);
                }
            }
        }
    },
    // General purpouse hooks that fire after all the rest is initialized.
    afterHooks: {
        client: {
            autoFormHooks: function () {
                AutoForm.hooks({
                    articlesForm: {
                        onSuccess: function () {
                            Router.go('articleList');
                        }
                    }
                });
            }
        }
    },
    // General purpouse hooks that fire before all the rest is initialized.
    beforeHooks: {
        client: {
            someHook: function () {
                // do stuff
            }
        }
    }
};